# Contributing to the Morning Music Bot 

First off, thanks for taking the time to contribute!

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report. Following these guidelines helps maintainers and the community understand your report :pencil:, reproduce the behavior :computer: :computer:, and find related reports :mag_right:.

Before creating bug reports, please perform a cursory search in the [GitLab issue list](https://gitlab.com/sealy/mm_bot/-/issues) to see if the problem has already been reported. If it has **and the issue is still open**, add a comment to the existing issue instead of opening a new one. If you find a **closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

When you are creating a bug report, please include as many details as possible:

- A clear and descriptive title
- Detailed steps to reproduce 
- Expected behavior
- Actual behavior
- Screenshot demonstrating the problem, if applicable
- If you're using the production deployment (on sd42.nl) or a development instance (please list the Git SHA)
- Anything else that may be relevant


### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion, including completely new features and minor improvements to existing functionality. Following these guidelines helps maintainers and the community understand your suggestion :pencil: and find related suggestions :mag_right:.

Before creating enhancement suggestions, please perform a cursory search in the [GitLab issue list](https://gitlab.com/sealy/mm_bot/-/issues?scope=all&state=alls) to see if the feature has already been proposed before. Also include **closed** issues in your search. If the issue is still **open**, you may want to add your specific reasons for wanting the feature as a comment. If it is **closed**, the comments will hopefully explain why this feature was not implemented. If you have new information that may change that decision, you can open a new issue and include a link to the original issue in the body of your new one.

When you are creating a feature request, please include as many details as possible:

- A clear and descriptive title
- Summary
- Motivation
- Anything else that may be relevant


### Contributing code

#### Picking an issue to work on

Unsure where to begin contributing to LMS42? You can start by looking through the `first-issue` and `help-wanted` issues:

* [First issues][https://gitlab.com/sealy/mm_bot/-/issues/?label_name%5B%5D=first_issue] - issues which should only require a few lines of code, and a test or two.
* [Help wanted issues][https://gitlab.com/sealy/mm_bot/-/issues/?label_name%5B%5D=help-wanted] - issues which should be a bit more involved than `beginner` issues.

When you start working on an issue, you should let others know about this in a comment, to prevent duplicate work. If work takes a bit longer, a status update may also be appreciated. If your attempt doesn't pan out, please put your findings in a comment as well, so others know that you're no longer working on this and can learn from your experience.

If you want to work on something for which no issue exists yet, you are highly encouraged to create a bug report or enhancement issue first. This allows others to react with additional insights to your plans, perhaps saving you from a lot of fruitless labour.


#### Getting started with the source code

The [README.md](README.md) contains the most important information on getting your development environment up and running. It also provides you with a brief tour of the directory structure, which will hopefully help you get your bearings quickly. Documentation could most definitely see some improvement though (hint!), but in the mean time you should feel free to liberally ask questions to the core contributors. Preferably, this should be done using an `@mention` in a comment to the related issue or merge request.

#### Merge Requests

Once you have managed to written a piece of codes that resolves the issue you're working on you should:

1. Make sure the tests (`poetry run python main_tests.py`) are happy.
2. Make sure your code is in line with the [styleguides](#styleguides).
3. Make sure your changes are in a single git *commit* with a descriptive commit message (or in multiple such commits, if there are a lot of changes).
4. Push your changes to a new *feature branch* created specifically for this issue.
5. Using the GitLab UI, create a *Merge Request* for this branch targeting the `sealy/mm_bot:master` branch. Within the description, include the phrase `Closes #123.` where `123` is the number of the issue this Merge Request is meant to resolve.
6. If the `mm_bot:master` branch contains commits your feature branch does not, you should *rebase* the latter.
7. Patiently await feedback from a core contributor.


## Styleguides

### Git Commit Messages

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and Merge Requests liberally after the first line


### Python

#### Indentation

Use 4 spaces per indentation level.

#### Maximum Line Length

Limit all lines to a maximum of 110 characters. Try the avoid the use of continuation lines, by splitting your statement into multiple statements (creating helper variables or functions).

#### One statement per line

While some compound statements such as list comprehensions are allowed and appreciated for their brevity and their expressiveness, it is bad practice to have two disjointed statements on the same line of code.

##### Bad

```python
print('one'); print('two')

if x == 1: print('one')

if <complex comparison> and <other complex comparison>:
    # do something
```

##### Good

```python
print('one')
print('two')

if x == 1:
    print('one')

cond1 = <complex comparison>
cond2 = <other complex comparison>
if cond1 and cond2:
    # do something
```

#### Blank Lines

Surround top-level function and class definitions with three blank lines.

Method definitions inside a class are surrounded by two blank line.

Within a function or method, logically coherent blocks of code should be separated by a single blank line. 

#### Comments

Comments should generally be used for:

- Introducing in a single line *what* the coherent block of code immediately below it does, like a paragraph title. This is not always necessary as it may already be self-evident at a glance.
- Explaining *why* something is implemented the way it is implemented. This is only necessary when the reasoning is non-obvious.

#### Inline documentation

At least all functions and methods that may be called from within another file should have a docstring describing its semantics, any arguments, any exceptions that can be thrown and the return value.

