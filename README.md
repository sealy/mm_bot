# mm_bot

A Discord bot that handles music requests.

## Getting started
Install the required packages by running:
```sh
poetry install
```

Note: we are using a development version of the py-cord library for our Discord client (which could possibly break the app at some point).

Run the bot as follows:
```sh
poetry run python main.py
```


Be sure to properly configure your discord server so the bot can access the channels, messages and create threads. Details can be found here: https://www.writebots.com/how-to-make-a-discord-bot/

In this repository you will find a *.env* file which contains the following environment variables needed for the bot to run:
```sh
DISCORD_SECRET=<your discord server secret>
DISCORD_CHANNELS=<channel to listen on for messages>
ADMIN_ROLE=<Admin role in your server to be able to use admin commands>
```

Note: You should provide a correct Discord secret so the bot can connect to your server. Never commit your secret to this repository! The default admin role is 'Teacher' (which should be a role on your Discord server). You can change this to your own role  if you want.

## Modes


### Birthday mode
When celebrating a someone's birthday:
```sh
poetry run python main.py --birthday <optional name of person to celebrate>
```
This way you can use the bot like normal, yet it selects a random birthday song to surprise everyone!


### Stealth mode
When you want the bot in stealth mode (Not being able to see what songs are added):
```sh
poetry run python main.py --stealth
```

When you want the bot in ultra stealth mode (not being able to see what songs are added neither be able to see the list of requests using the `!mm` command):
```sh
poetry run main.py --stealth ultra
```

PRO Tip: When users submit their request the message is deleted by the bot (when in it is configured in stealth mode). Previews for links are created allowing others to see the preview for a fraction of a second. To prevent this users can submit their request using a spoiler tag (also see https://support.discord.com/hc/en-us/articles/360022320632-Spoiler-Tags-) & Disable users being able to embed links in the channel.


## Commands
To add a song to the request list:
```sh
Within the created thread users can run the following command "!mm_add <song_link | song_title>" to add a song to the request list.
```

To reject / remove a song out the list of requests:
```sh
Within the created thread in discord run the following command "!mm_reject <index>"
This removes the song on given index in "!mm" requests list.
```

To make a final decision on which song to play:
```sh
Run within the created thread the command "When the crowd says bot!"
This returns a message with a link to the selected song.
```


## Authors and acknowledgment
People of [SD42](https://sd42.nl).


## Project status
This project is a purely for fun. Don't expect too much maintenance.