import sqlite3
from . import queries
from random import choice



########## INITIALIZE ##########

def connect(db_location):
    connection = sqlite3.connect(db_location)
    connection.row_factory = sqlite3.Row

    return connection

def init_db(db_location):
    connection = connect(db_location)

    connection.execute(queries.CREATE_USERS_TABLE)
    connection.execute(queries.CREATE_REQUESTS_TABLE)
    connection.execute(queries.CREATE_HISTORY_TABLE)
    connection.execute(queries.CREATE_BLACKLIST_TABLE)
    connection.execute(queries.CREATE_BIRTHDAY_SONGS_TABLE)
    connection.execute(queries.CREATE_SUGGESTION_TABLE)

    connection.commit()

    return connection



########## INSERT ##########

def insert_user(connection, discord_id, nick_name, level):
    connection.execute(queries.INSERT_USER, (discord_id, nick_name, level))
    connection.commit()

def insert_request(connection, discord_id, yt_title, yt_link):
    connection.execute(queries.INSERT_REQUEST, (discord_id, yt_title, yt_link))
    connection.commit()

def insert_history(connection, request_id):
    connection.execute(queries.INSERT_HISTORY, (request_id,))
    connection.commit()

def insert_blacklist(connection, yt_title, yt_link):
    connection.execute(queries.INSERT_BLACKLIST, (yt_title, yt_link))
    connection.commit()    

def insert_birthday_song(connection, yt_title, yt_link):
    connection.execute(queries.INSERT_BIRTHDAY_SONG, (yt_title, yt_link))
    connection.commit()

def insert_suggestion(connection, discord_id, song_title):
    connection.execute(queries.INSERT_SUGGESTION, (discord_id, song_title))
    connection.commit()


########## SELECT ##########
def get_current_suggestions(connection, discord_user_id):
    return connection.execute(queries.GET_SUGGESTIONS_BY_DISCORD_ID, (discord_user_id,)).fetchall()

def get_previous_request(connection, discord_id, amount=5):
    return connection.execute(queries.SELECT_LAST_REQUESTS, (discord_id, amount)).fetchall()

def get_history(connection):  
    return connection.execute(queries.SELECT_ALL_HISTORY).fetchall()

def already_played(connection, yt_link):
    result = connection.execute(queries.SELECT_HISTORY_BY_LINK, (yt_link,)).fetchall()
    return len(result) == 1

def in_blacklist(connection, yt_link):
    result = connection.execute(queries.SELECT_BLACKLIST_BY_LINK, (yt_link,)).fetchall()
    return len(result) == 1

def get_birthday_song(connection):
    result = connection.execute(queries.SELECT_BIRTHDAY_SONGS).fetchall()
    return choice(result)

def user_exists(connection, discord_id):
    result = connection.execute(queries.SELECT_USER_BY_DISCORD_ID, (discord_id,)).fetchall()
    return len(result) == 1

def get_current_requests(connection, check_if_any=False):
    result = connection.execute(queries.GET_CURRENT_REQUESTS).fetchall()

    if check_if_any:    
        return len(result) > 0
    return result

def get_users_by_weights(connection):
    return connection.execute(queries.GET_USERS_BY_WEIGHTS).fetchall()


def get_user_request(connection, discord_id):
    return connection.execute(queries.GET_REQUESTED_SONG, (discord_id,)).fetchone()



########## UPDATE ##########

def update_user_weight(connection, participants):
    for participant in participants:
        connection.execute(queries.INCREMENT_USER_WEIGHT, (participant['id'],))
    connection.commit()

def reset_user_weight(connection, discord_id):
    connection.execute(queries.RESET_USER_WEIGHT, (discord_id,))
    connection.commit()



########## DELETE ##########

def delete_request(connection, request_id):
    connection.execute(queries.DELETE_REQUEST, (request_id,))
    connection.commit()