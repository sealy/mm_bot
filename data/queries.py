########## CREATE TABLE QUERIES ##########

CREATE_USERS_TABLE = '''create table if not exists users(
  id integer primary key,
  discord_id text not null unique,
  nick_name text not null,
  weight integer not null default 1,
  level int not null default 10 /* 10=user 20=admin 30=bot */
);'''

CREATE_REQUESTS_TABLE = '''create table if not exists requests(
    id integer primary key,
    user_discord_id integer not null,
    yt_title text not null,
    yt_link text not null,
    created_on datetime not null default (datetime('now','localtime')),
    FOREIGN KEY(user_discord_id) REFERENCES users(discord_id)
);'''

CREATE_HISTORY_TABLE = '''create table if not exists history(
    id integer primary key,
    request_id integer not null,
    selected_on datetime not null default (datetime('now','localtime')),
    FOREIGN KEY(request_id) REFERENCES requests(id)
);'''

CREATE_BLACKLIST_TABLE = '''create table if not exists blacklist(
    id integer primary key,
    yt_title text not null,
    yt_link text not null
);'''

CREATE_BIRTHDAY_SONGS_TABLE = '''create table if not exists birthday_songs(
    id integer primary key,
    yt_title text not null,
    yt_link text not null
);'''

CREATE_SUGGESTION_TABLE = '''CREATE TABLE IF NOT EXISTS suggestions (
    id              INTEGER  PRIMARY KEY,
    user_discord_id INTEGER  NOT NULL,
    song_title      TEXT     NOT NULL,
    created_on      DATETIME NOT NULL DEFAULT (datetime('now', 'localtime') ),
    FOREIGN KEY (user_discord_id) REFERENCES users (discord_id) 
);'''

########## INSERT ##########

INSERT_USER = "INSERT INTO users (discord_id, nick_name, level) VALUES (?,?,?);"

INSERT_REQUEST = "INSERT INTO requests (user_discord_id, yt_title, yt_link) VALUES (?,?,?);"

INSERT_HISTORY = "INSERT INTO history (request_id) VALUES (?);"

INSERT_BLACKLIST = "INSERT INTO blacklist (yt_title, yt_link) VALUES (?,?);"

INSERT_BIRTHDAY_SONG = "INSERT INTO birthday_songs (yt_title, yt_link) VALUES (?,?);"

INSERT_SUGGESTION = "INSERT INTO suggestions (user_discord_id, song_title) VALUES (?,?);"


########## SELECT ##########
SELECT_LAST_REQUESTS = '''
SELECT DISTINCT r.yt_title as title, r.yt_link as link
FROM requests as r
WHERE r.user_discord_id = ?
ORDER BY r.created_on 
DESC LIMIT ?;
'''


SELECT_ALL_HISTORY = '''SELECT u.nick_name, r.yt_title 
FROM history as h
INNER JOIN requests as r ON h.request_id = r.id
INNER JOIN users as u ON r.user_discord_id = u.discord_id
ORDER BY selected_on DESC;'''

SELECT_HISTORY_BY_LINK = '''SELECT * 
FROM history as h 
INNER JOIN requests as r ON h.request_id = r.id 
WHERE r.yt_link = ?;'''

SELECT_BLACKLIST_BY_LINK = "SELECT * FROM blacklist WHERE yt_link = ?;"

SELECT_BIRTHDAY_SONGS = "SELECT * FROM birthday_songs;"

SELECT_USER_BY_DISCORD_ID = "SELECT * FROM users WHERE discord_id = ?;"

GET_CURRENT_REQUESTS = '''SELECT r.id as id, u.nick_name as name, u.discord_id as discord_id, u.weight as weight, r.yt_title as yt_title, r.yt_link as yt_link 
FROM requests as r
INNER JOIN users as u ON r.user_discord_id = u.discord_id
WHERE date(r.created_on) = date(datetime('now','localtime'));'''

GET_USERS_BY_WEIGHTS = "SELECT * FROM users ORDER BY weight DESC;"

GET_REQUESTED_SONG = "SELECT * FROM requests WHERE user_discord_id = ? AND date(created_on) = date(datetime('now','localtime'));"

GET_SUGGESTIONS_BY_DISCORD_ID = "SELECT * FROM suggestions WHERE user_discord_id = ? AND date(created_on) = date(datetime('now','localtime'));"


########## UPDATE ##########

INCREMENT_USER_WEIGHT = "UPDATE users SET weight = (weight + 1) WHERE discord_id = ?;"
RESET_USER_WEIGHT = "UPDATE users SET weight = 1 WHERE discord_id = ?"
UPDATE_USER_WEIGHT = "UPDATE users SET weight = ? WHERE discord_id = ?;"


########## DELETE ##########

DELETE_REQUEST = "DELETE FROM requests where id = ?;"