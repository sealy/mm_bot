from enum import Enum
import requests


class RequiredArgument(Exception):

    def __init__(self, argument, method, *args):
        super().__init__(args)
        self._argument = argument
        self._method = method

    def __str__(self):
        return f"Argument '{self._argument}' is required in method '{self._method}'."


class RequestMethod(Enum):
    GET = 1
    POST = 2


class DiscordExtension:
    __BASE_URL = "https://discord.com/api/v9/"

    def __init__(self, secret):
        self.__secret = secret


    def __do_request(self, url, method: RequestMethod, data = None):
        """
        This function is used to make (REST) API requests for the discord bot. It will return the response for the request.
        @param self - the DiscordExtension object
        @param url - the (relative) url for which the request should be made
        @param method - the type of request (GET or POST) which should be made
        @param data - the optional data payload (a dictionary) to send with the request 
        @returns the response (in JSON format) received for the supplied request
        """
        headers = {
            "authorization" : f"Bot {self.__secret}",
            "content-type" : "application/json"
        }
        if method == RequestMethod.GET:
            return requests.get(url,headers=headers).json()
        elif method == RequestMethod.POST:
            return requests.post(url,headers=headers,json=data).json()


    def get_threads(self, channel):
        """
        Get the threads for a channel.
        @param self - the DiscordExtension object
        @param channel - the channel to get the threads for
        @returns the threads for the channel
        """
        if not channel:
            raise RequiredArgument('channel', 'DiscordExtension.get_threads()')
        url = f"{DiscordExtension.__BASE_URL}/channels/{channel.id}/threads/active"
        response = self.__do_request(url, RequestMethod.GET)
        return response["threads"]


    def create_thread(self, name, channel, auto_archive_duration = 60):
        """
        Create a thread for the channel.
        @param name - the name of the thread
        @param channel - the channel to create the thread in
        @param auto_archive_duration - the duration of the thread in minutes
        """
        if not name:
            raise RequiredArgument('name', 'DiscordExtension.create_thread()')
        if not channel:
            raise RequiredArgument('channel', 'DiscordExtension.create_thread()')

        url = f"{DiscordExtension.__BASE_URL}channels/{channel.id}/threads"
        data = {
            "name" : name,
            "type" : 11,
            "auto_archive_duration" : auto_archive_duration
        }
        response = self.__do_request(url, RequestMethod.POST, data)
        if "message" in response.keys():
            raise Exception("Error creating thread: " + response["message"])
        else:
            return response
    
    