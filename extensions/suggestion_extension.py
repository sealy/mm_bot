import requests
import base64
from data import db


class SuggestionExtension():
    TOKEN_ENDPOINT = "https://accounts.spotify.com/api/token"
    API_BASE_URL = "https://api.spotify.com/v1"
    MAX_SONG_DURATION = 300000
    MAX_SUGGESTIONS = 5
    
    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret


    def _get_auth_token(self):
        secret = base64.b64encode(f"{self.client_id}:{self.client_secret}".encode("ascii")).decode('ascii')
        payload = "grant_type=client_credentials"
        headers = {
            "Authorization": 
            f"Basic {secret}", 
            "content-type": 
            "application/x-www-form-urlencoded"
        }
        res = requests.post(SuggestionExtension.TOKEN_ENDPOINT, data=payload, headers=headers).json()
        return f"Bearer {res['access_token']}"
    

    def request_suggestion(self, connection, user_id):
        # Check if we already have suggestions for today
        previous_suggestions = db.get_current_suggestions(connection, user_id)
        if len(previous_suggestions) > 0:
            return [suggestion['song_title'] for suggestion in previous_suggestions]

        # If not fetch the previous request so we can send some song titles to the suggest api.
        previous_tracks = [track[0] for track in db.get_previous_request(connection, user_id)]
        seed_artists = []
        headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "authorization": self._get_auth_token(),
            "Content-Type": "application/json"
        }
        # Get the (spotify) artist id for every track. 
        for track in previous_tracks:
            try:
                res = requests.get(f"{SuggestionExtension.API_BASE_URL}/search?q={track}&type=track&limit=1", headers=headers).json()
                suggestions = res["tracks"]["items"][0]
                artist_id = suggestions["album"]["artists"][0]["id"]
                seed_artists.append(artist_id)
            except IndexError:
                # Skips if no track was found
                pass
                
        processed_suggestions = []
        seed_artists = ",".join(seed_artists)
        url = f"{SuggestionExtension.API_BASE_URL}/recommendations?seed_artists={seed_artists}&seed_genres=&limit={SuggestionExtension.MAX_SUGGESTIONS}&max_duration_ms={SuggestionExtension.MAX_SONG_DURATION}"
        res = requests.get(url, headers=headers).json()
        for track in res["tracks"]:
            song_title = f"{track['name']} - {track['artists'][0]['name']}"
            processed_suggestions.append(song_title)
            db.insert_suggestion(connection, user_id, song_title)
        return processed_suggestions