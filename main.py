import os
import sys
from data import db

import discord
from dotenv import load_dotenv

import mm_processors as proc
from extensions.discord_extension import DiscordExtension
from extensions.suggestion_extension import SuggestionExtension

load_dotenv()

MAX_MINUTES = 5
PROFILE_PICTURE = 'profile_picture'
ADMIN_ROLE = os.environ.get('ADMIN_ROLE','Teacher')
DISCORD_SECRET = os.environ.get('DISCORD_SECRET','')
DISCORD_CHANNELS = os.environ.get('DISCORD_CHANNELS', '').split(',')
DB_LOCATION = os.environ.get('DB_LOCATION', './data/mm.db')
SPOTIFY_CLIENT_ID = os.environ.get('SPOTIFY_CLIENT_ID','')
SPOTIFY_CLIENT_SECRET = os.environ.get('SPOTIFY_CLIENT_SECRET','')


class MusicRequestMessage():
    """Represents a music request message.

    This class takes the original message string in the constructor and tries to determine whether the message is music request message. (Most) music request messages start with '!'. 
    """

    SPOILER_MARKER = "||"

    def __init__(self, message):
        self._message = message
        # Unpack spoiler message for further processing.
        MARK = MusicRequestMessage.SPOILER_MARKER
        if self._message.startswith(MARK) and self._message.endswith(MARK):
            self._message = self._message[len(MARK):-len(MARK)]

    @property
    def message_type(self):
        """Tries to determine whether this message is a music request method (which start with a '!').

        Returns:
            string: the first part of the message if it starts with a '!' or the original message.  
                
        For example if the original message was '!mm_add <url>' then '!mm_add' is returned.
        """
        
        if self._message.startswith("!"):
            # !mm messages
            return self._message.split(" ")[0]
        else:
            # When the crowd says bot!
            return self._message


class MyClientConfig():
    """Represents the configuration of the client.

    The configuration contains aspects like:
    * the mode the client is in (birthday, stealth or ultra stealth)
    * the (optional) name of the party animal (in case the client is set to birthday mode)
    * the (discord) channels to list for incoming messages (read from a .env file)
    * the (discord) role for the super user (read from a .env file)
    * the path of the (SQLite) database file (read from a .env file)
    """

    def __init__(self, 
        birthday=False, 
        party_animal="the party animal", 
        stealth_request= False, 
        ultra_stealth=False,
        auto_init = False
    ):
        self.auto_init = auto_init
        self.birthday = birthday
        self.party_animal = party_animal
        self.stealth_request = stealth_request
        self.ultra_stealth = ultra_stealth
        self.channels = DISCORD_CHANNELS
        self.admin_role = ADMIN_ROLE
        self.db_path = DB_LOCATION

    @property
    def mode(self):
        """A string representation of the configured mode.

        Returns:
            string: A human readable representation of the configured mode. 
        """
        if self.stealth_request:
            if self.ultra_stealth:
                return "ultra stealth"
            else:
                return "stealth"
        return "normal"


class MyClient(discord.Client):
    """Represents the discord client that implements the bot's logic.
    """

    BIRTHDAY_SONGS = [
        ('Hèy Kanjer, Dit is je verjaardag - Hartelijk gefeliciteerd !', 'https://www.youtube.com/watch?v=Us_hcts08UQ'), 
        ('Gefeliciteerd', 'https://www.youtube.com/watch?v=_6avS5pxeVQ'), 
        ('Hi Ha hartelijk gefeliciteerd 🎵 Verjaardagsliedjes 🎈 Nederlands', 'https://www.youtube.com/watch?v=zBnGmvk7GZA'), 
        ('Van harte gefeliciteerd met je verjaardag!', 'https://www.youtube.com/watch?v=Sq9jQNLpCHM'), 
        ('Funny Happy Birthday Song. Monkeys sing Happy Birthday To You','https://www.youtube.com/watch?v=HvYgke2fBeg'),
        ('DE VERJAARDAGSHIT! VAN HARTE GEFELICITEERD OMDAT JIJ JARIG BENT!', 'https://www.youtube.com/watch?v=DxlROy6FlqY')
    ]


    def __init__(self, config=MyClientConfig()):
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(intents=intents)
        self.discord_extension = DiscordExtension(DISCORD_SECRET)
        self.suggestion_extension = SuggestionExtension(SPOTIFY_CLIENT_ID, SPOTIFY_CLIENT_SECRET)
        self.config = config
        self.current_thread = None
        self.connection = None
        self.__processors = [
            proc.MusicRequestListProcessor(self),
            proc.MusicRequestAddProcessor(self),
            proc.MusicRequestRepeatProcessor(self),
            proc.MusicRequestSuggestProcessor(self),
            proc.MusicRequestEndorseProcessor(self),
            proc.MusicRequestInitProcessor(self),
            proc.MusicRequestRejectProcessor(self),
            proc.MusicRequestWeightsProcessor(self),
            proc.MusicRequestSimilarityProcessor(self),
            proc.MusicRequestSelectProcessor(self),
        ]


    @property
    def mapping(self):
        return { processor.command: processor for processor in self.__processors }


    @property
    def requests(self):
        return { 
            r["discord_id"]: { 
                "id": r["id"],
                "name": r["name"], 
                "weight": r["weight"], 
                "discord_id": r["discord_id"],
                "title": r["yt_title"], 
                "link": r["yt_link"]  
                } for r in db.get_current_requests(self.connection)
            }
        

    def _create_connection(self, client_user):
        if os.path.exists(self.config.db_path):
            self.connection = db.connect(self.config.db_path)
        else:
            # Create database and initialize it with birthday songs.
            self.connection = db.init_db(self.config.db_path)
            db.insert_user(self.connection, client_user.id, client_user.name, 30)

            for song in MyClient.BIRTHDAY_SONGS:
                db.insert_birthday_song(self.connection, song[0], song[1])


    async def on_ready(self):
        print('Logged on as', self.user)
        self._create_connection(self.user)
        print('Database connection created!')
    
    
    async def on_message(self, message):
        """"
        Process the message and send the result back to the channel.
        
        Args:
            message (string): the message to process.
        """

        # Don't respond to ourselves (or other channels than we have configured)
        if message.author == self.user:
            return
        
        if message.channel.name not in self.config.channels \
            and message.channel.name != self.current_thread:
            print(f'Not responding in channel {message.channel}')
            return
        
        # Random stuff
        msg = message.content
        if msg.lower() == 'ching':
            await message.channel.send('chong')

        if msg.lower() == 'chong':
            for role in message.author.roles:
                if role.name == self.config.admin_role:
                    await message.channel.send('schwing')
                    break
            else:
                await message.channel.send('ching')
            
        # Actual message processing
        music_req = MusicRequestMessage(msg)
        if music_req.message_type in self.mapping:
            result = await self.mapping[music_req.message_type].process(message)
            if result:
                await result.send(message.channel)
            return


def parse_arguments(args):
    clientConfig = MyClientConfig()
    args_index = 0
    while True:
        if args_index == len(args):
            break
        match args[args_index]:
            case '--auto-init':
                clientConfig.auto_init = True
            case '--birthday':
                clientConfig.birthday = True
                if args_index+1 < len(args) and not args[args_index+1].startswith('--'):
                    args_index += 1
                    clientConfig.party_animal = args[args_index]
            case '--stealth':
                clientConfig.stealth_request = True
                if args_index+1 < len(args) and not args[args_index+1].startswith('--') and args[args_index+1] == "ultra":
                    args_index += 1
                    clientConfig.ultra_stealth = True
        args_index += 1
    return clientConfig


if __name__ == "__main__":
    client = MyClient(parse_arguments(sys.argv))
    client.run(DISCORD_SECRET)