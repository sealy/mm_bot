
from random import randint


class MockDiscordExtension():
    DEBUG_PRINT = False
    MAX_GUILD_ID = 100
    MAX_THREAD_ID = 100

    def __init__(self):
        self.threads = {}

    @staticmethod
    def log(message):
        if MockDiscordExtension.DEBUG_PRINT:
            print(f"MOCK: {message}")

    def get_threads(self, channel):
        MockDiscordExtension.log(f"calling get_threads on {channel}")
        return self.threads.get(channel.name, [])

    def create_thread(self, name, channel, auto_archive_duration = 60):
        MockDiscordExtension.log(f"calling create_thread with name {name} on {channel}")
        if not channel.name in self.threads:
            self.threads[channel.name] = []
        self.threads[channel.name].append(MockDiscordThread(channel, name))
        mock_response = {
            'name': name,
            'guild_id': randint(0, MockDiscordExtension.MAX_GUILD_ID),
            'id': randint(0, MockDiscordExtension.MAX_THREAD_ID)
        }
        return mock_response


class MockDiscordThread():
    def __init__(self, channel, name):
        self.channel = channel
        self.content = name


class MockDiscordMessage():
    def __init__(self, channel, content, author):
        self.channel = channel
        self.content = content
        self.author = author
        self.deleted = False

    async def delete(self):
        self.deleted = True


class MockDiscordChannelSendException(Exception):
    def __init__(self, message, payload=None):
        self.message = message
        self.payload = payload


class MockDiscordChannel():
    def __init__(self, name):
        self.name = name
    

    async def send(self, message=None, *, embed=None):
        if embed:
            MockDiscordExtension.log(f"Sending embed message to channel {self.name}")
            raise MockDiscordChannelSendException(f"MOCK: Embed message", payload=embed)
        MockDiscordExtension.log(f"Sending message {message} to channel {self.name}")
        raise MockDiscordChannelSendException(f"MOCK: {message}")

 

class MockDiscordMessageAuthor():
    __AUTHOR_ID = 0

    def __init__(self, name, roles):
        MockDiscordMessageAuthor.__AUTHOR_ID += 1
        self.id = str(MockDiscordMessageAuthor.__AUTHOR_ID)
        self.name = name
        self.nick = None
        self.roles = roles


class MockDiscordRole():
    def __init__(self, name):
        self.name = name


class MockDiscordUser():
    def __init__(self, id, name):
        self.id = id
        self.name = name
