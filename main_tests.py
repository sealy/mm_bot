import unittest
import os
from datetime import datetime
from main import MyClient, parse_arguments
from main_test_mocks import *
from mm_processors import WeightedSelection

class ConfigurationTest(unittest.TestCase):
    DEFAULT_PARTY_ANIMAL = "the party animal"
    
    def test_parse_no_arguments(self):
        config = parse_arguments([])
        self.assertFalse(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertFalse(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_birthday_only(self):
        config = parse_arguments(["--birthday"])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertFalse(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_birthday_party_animal(self):
        party_animal = "Alice"
        config = parse_arguments(["--birthday", party_animal])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, party_animal)
        self.assertFalse(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_birthday_invalid_party_animal(self):
        party_animal = "Alice"
        config = parse_arguments([party_animal, "--birthday"])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertFalse(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_stealth_only(self):
        config = parse_arguments(["--stealth"])
        self.assertFalse(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertTrue(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_utlra_stealth_only(self):
        config = parse_arguments(["--stealth", "ultra"])
        self.assertFalse(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertTrue(config.stealth_request)
        self.assertTrue(config.ultra_stealth)


    def test_parse_stealth_birthday(self):
        config = parse_arguments(["--birthday", "--stealth"])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertTrue(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


        config = parse_arguments(["--stealth", "--birthday"])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertTrue(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_stealth_birthday_party_animal(self):
        party_animal = "Alice"
        config = parse_arguments(["--birthday", party_animal, "--stealth"])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, party_animal)
        self.assertTrue(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


    def test_parse_stealth_birthday_invalid_party_animal(self):
        party_animal = "Alice"
        config = parse_arguments(["--birthday", "--stealth", party_animal])
        self.assertTrue(config.birthday)
        self.assertEqual(config.party_animal, ConfigurationTest.DEFAULT_PARTY_ANIMAL)
        self.assertTrue(config.stealth_request)
        self.assertFalse(config.ultra_stealth)


class ClientTest(unittest.IsolatedAsyncioTestCase):
    DB_PATH = "./data/mm_test.db"
    SONG_REQUEST = "Rick Astley - Never gonna give you up"
    TEST_CHANNEL = "morning_music"
    TEST_AUTHOR = "Tester"
    TEST_ADMIN = "admin"
    TEST_ADMIN_ROLE = "admin"
    TEST_NON_ADMIN_ROLE = "chef_hr"
    TEST_MM_EMBED_MESSAGE = "Embed message" 
    TEST_MM_INIT_ERROR_MESSAGE = "👮🏻‍♂️: Computer says no!"
    TEST_MM_ADD_SUCCESS_MESSAGE = "Great success!" 
    TEST_MM_ADD_ERROR_MESSAGE = "Great success! - NOT" 
    TEST_MM_ADD_NO_INIT_ERROR_MESSAGE = "Please submit your requests in thread channel"
    TEST_MM_REJECT_EMPTY_LIST_ERROR_MESSAGE = "There are no songs to reject!"
    TEST_MM_REJECT_SUCCESS_MESSAGE = "And its gone..."
    TEST_MM_REJECT_ERROR_MESSAGE = "⛔️ This is not a valid index"
    TEST_MM_EMPTY_LIST_MESSAGE = "No requests have been made..."
    TEST_MM_REPEAT_ERROR_MESSAGE = "Such empty... 🤷🏽‍♂️" 
    TEST_MM_REPEAT_SUCCESS_MESSAGE = "🔁 Great success!"
    TEST_MM_ULTRA_STEALTH_MESSAGE = "Nothing to see here. Move along..."
    TEST_MM_ENDORSE_EMPTY_LIST_MESSAGE = "There are no songs to endorse!"
    TEST_MM_ENDORSE_INVALID_ERROR_MESSAGE = "⛔️ This is not a valid index"
    TEST_MM_ENDORSE_DUP_ERROR_MESSAGE = "⛔️ You have already requested a song (so you cannot endorse another)"
    TEST_MM_ENDORSE_SUCCESS_MESSAGE ="🔗 Great success! - You have endorsed"
    TEST_MM_LIST_TITLE = "Requests"


    def setUp(self):
        self.client = MyClient()
        self.client.config.admin_role = ClientTest.TEST_ADMIN_ROLE
        self.client.config.channels.append(ClientTest.TEST_CHANNEL)
        self.client.config.db_path = ClientTest.DB_PATH
        self.client.discord_extension = MockDiscordExtension()
        self.client._create_connection(MockDiscordUser(1, "mm_bot"))

        self.author = MockDiscordMessageAuthor(ClientTest.TEST_AUTHOR, [])
        self.admin = MockDiscordMessageAuthor(
            ClientTest.TEST_ADMIN, 
            [MockDiscordRole(ClientTest.TEST_ADMIN_ROLE)]
        )


    def tearDown(self):
        os.remove(ClientTest.DB_PATH)
        self.client = None
        self.author = None
        self.admin = None


    async def test_client_ching_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='ching',
                author=self.author
            ))
        self.assertTrue(f'MOCK: chong' in str(context.exception))


    async def test_client_chong_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='chong',
                author=self.author
            ))
        self.assertTrue(f'MOCK: ching' in str(context.exception))


    async def test_client_admin_chong_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='chong',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: schwing' in str(context.exception))


    async def test_client_mm_init_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)


    async def test_client_mm_init_no_permission(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_INIT_ERROR_MESSAGE}' in str(context.exception))


    async def test_client_mm_add_no_init_fail(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_NO_INIT_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})


    async def test_client_mm_add_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)


    async def test_client_mm_add_fail(self):
        INVALID_SONG_REQUEST = "https://www.youtube.com/watch?v=fJ9rUzIMcZQ"
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {INVALID_SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})


    async def test_client_mm_add_success_birthday_mode(self):
        self.client.config.birthday = True
        self.client.config.party_animal = ClientTest.TEST_AUTHOR
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        request_message = MockDiscordMessage(
            channel=request_channel,
            content=f'!mm_add {ClientTest.SONG_REQUEST}',
            author=self.admin
        )
        with self.assertRaises(Exception) as context:
            await self.client.on_message(request_message)
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        # When the crowd says bot!
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'When the crowd says bot!',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: SELECTA: Happy Birthday to {ClientTest.TEST_AUTHOR}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        request_url = self.client.requests[request_message.author.id]['link']
        selected_url = str(context.exception).split(" - ")[1]
        self.assertNotEqual(selected_url, request_url)
        self.assertIn(selected_url, [song[1] for song in MyClient.BIRTHDAY_SONGS])


    # # # TODO: Test for add when no video can be found.


    async def test_client_mm_reject_empty_list(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Reject the song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_reject 1',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_REJECT_EMPTY_LIST_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})


    async def test_client_mm_reject_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Add a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        # Reject the song
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_reject 1',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_REJECT_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})


    async def test_client_mm_reject_fail(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Add a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        # Reject the wrong song
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_reject 2',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_REJECT_ERROR_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        # Reject the song without permission
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_reject 1',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_INIT_ERROR_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)


    async def test_client_mm_empty_list(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Request list
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content=f'!mm',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMPTY_LIST_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})


    async def test_client_mm_list(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Add a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(len(self.client.requests), 1)
        # Request list
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm',
                author=self.author
            ))
        mm_list = '\n'.join([f"{index+1}. {r['name']} ({100 // (1 * len(self.client.requests))}%): ***{r['title']}***" for index, r in enumerate(self.client.requests.values())])
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.fields[0].name, ClientTest.TEST_MM_LIST_TITLE)
        self.assertEqual(context.exception.payload.fields[0].value, mm_list)
        self.assertTrue(len(self.client.requests), 1)


    async def test_client_mm_stealth(self):
        self.client.config.ultra_stealth = False
        self.client.config.stealth_request = True
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request (empty) list
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMPTY_LIST_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})
        # Add a song
        message = MockDiscordMessage(
            channel=request_channel,
            content=f'!mm_add {ClientTest.SONG_REQUEST}',
            author=self.admin
        )
        with self.assertRaises(Exception) as context:
            await self.client.on_message(message)
        self.assertTrue(f'MOCK: To {self.admin.name}: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertTrue(message.deleted)    
        self.assertTrue(len(self.client.requests), 1)
        # Request list (with one song)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm',
                author=self.author
            ))

        mm_list = '\n'.join([f"{index+1}. {r['name']} heeft een plaatje aangevraagd!" for index, r in enumerate(self.client.requests.values())])
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.fields[0].name, ClientTest.TEST_MM_LIST_TITLE)
        self.assertEqual(context.exception.payload.fields[0].value, mm_list)
        self.assertTrue(len(self.client.requests), 1)


    async def test_client_mm_ultra_stealth(self):
        self.client.config.ultra_stealth = True
        self.client.config.stealth_request = True
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request (empty) list
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ULTRA_STEALTH_MESSAGE}' in str(context.exception))
        self.assertEqual(self.client.requests, {})
        # Add a song
        message = MockDiscordMessage(
            channel=request_channel,
            content=f'!mm_add {ClientTest.SONG_REQUEST}',
            author=self.admin
        )
        await self.client.on_message(message)
        self.assertTrue(message.deleted)    
        self.assertTrue(len(self.client.requests), 1)
        # Request list (with one song)
        # Note: response should be the same.
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ULTRA_STEALTH_MESSAGE}' in str(context.exception))


    async def test_client_mm_repeat_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)
        # Send repeat request
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_repeat',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_REPEAT_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)


    async def test_client_mm_repeat_success_stealth(self):
        self.client.config.ultra_stealth = False
        self.client.config.stealth_request = True
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.author
            ))
        self.assertTrue(f'MOCK: To {self.author.name}: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)
        # Send repeat request
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_repeat',
                author=self.author
            ))
        self.assertTrue(f'MOCK: To {self.author.name}: {ClientTest.TEST_MM_REPEAT_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)


    async def test_client_mm_repeat_fail(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song repeat (when not previous request has been made)
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_repeat',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_REPEAT_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 0)


    async def test_client_mm_endorse_empty_list(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Endorse a song (when no requests have been made)
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_endorse',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ENDORSE_EMPTY_LIST_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 0)


    async def test_client_mm_endorse_when_already_requested(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)
        # Send endorse request (for yourself)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_endorse',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ENDORSE_DUP_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)


    async def test_client_mm_endorse_fail(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)
        # Send an invalid endorse request (no index)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_endorse',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ENDORSE_INVALID_ERROR_MESSAGE}' in str(context.exception))
        # Send an invalid endorse request (invalid index)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_endorse 2',
                author=self.admin
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ENDORSE_INVALID_ERROR_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)


    async def test_client_mm_endorse_success(self):
        self.assertEqual(self.client.requests, {})
        test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
        # Init
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=test_channel,
                content='!mm_init',
                author=self.admin
            ))
        thread_name = datetime.now().strftime('%Y%m%d')
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
        self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
        self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
        self.assertEqual(self.client.current_thread, thread_name)
        # Request a song
        request_channel = MockDiscordChannel(thread_name)
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_add {ClientTest.SONG_REQUEST}',
                author=self.author
            ))
        self.assertTrue(f'MOCK: {ClientTest.TEST_MM_ADD_SUCCESS_MESSAGE}' in str(context.exception))
        self.assertEqual(len(self.client.requests), 1)
        # Send endorse request
        with self.assertRaises(Exception) as context:
            await self.client.on_message(MockDiscordMessage(
                channel=request_channel,
                content=f'!mm_endorse 1',
                author=self.admin
            ))
        expected = f"{ClientTest.TEST_MM_ENDORSE_SUCCESS_MESSAGE} {self.client.requests[self.author.id]['title']}: {self.client.requests[self.author.id]['link']}"
        self.assertTrue(f'MOCK: {expected}' in str(context.exception))
        # HACK: This should be 2 but ... (we don't insert new users when endorsing so the request is not added)
        self.assertEqual(len(self.client.requests), 1)


    # async def test_client_mm_auto_init(self):
    #     self.client.config.auto_init = True
    #     self.assertEqual(self.client.requests, {})
    #     test_channel = MockDiscordChannel(ClientTest.TEST_CHANNEL)
    #     # Init
    #     with self.assertRaises(Exception) as context:
    #         await self.client.on_ready()
    #     thread_name = datetime.now().strftime('%Y%m%d')
    #     self.assertTrue(f'MOCK: {ClientTest.TEST_MM_EMBED_MESSAGE}' in str(context.exception))
    #     self.assertEqual(context.exception.payload.title, f"Listening on thread {thread_name}")
    #     self.assertTrue(len(self.client.discord_extension.get_threads(test_channel)), 1)
    #     self.assertEqual(self.client.current_thread, thread_name)


class WeightedSelectionTest(unittest.TestCase):
    FACTOR = 100
    NUM_PARTICIPANTS = 10
    THRESHOLD = 90
    ERROR_MARGIN = 5
    UNFAIR_ERROR_MESSAGE = "The weighted selection is not fair."
    STILL_UNFAIR_ERROR_MESSAGE= "The weighted selection is still not fair."
    DB_FILE_TEST = "mm_weights_test.data"
    
    def setup_client(self):
        client = MyClient()
        client.config.admin_role = ClientTest.TEST_ADMIN_ROLE
        client.config.channels.append(ClientTest.TEST_CHANNEL)
        client.discord_extension = MockDiscordExtension()
        client._create_connection(MockDiscordUser(1, "mm_bot"))
        return client


    def test_successful_select(self):
        INDEX_PRIVILEGED_PARTICIPANT = 0
        num_first_selected = 0
        num_first_selected_second_time = 0
        # Run a lot of selections to average out the weights bias.
        for _ in range(100 * WeightedSelectionTest.FACTOR):
            ws = WeightedSelection()
            participants = [ { "id": i, "weight": 1 } for i in range(WeightedSelectionTest.NUM_PARTICIPANTS)]
            participants[INDEX_PRIVILEGED_PARTICIPANT]["weight"] = 100

            # Add all test participants and override their weights
            for participant in participants:
                ws.add_participant(participant["id"], participant["weight"])

            # Make a first selection and in most cases (above threshold) it should be the first participant.
            if ws.select() == participants[INDEX_PRIVILEGED_PARTICIPANT]["id"]:
                num_first_selected +=1

            # Select again and in the second case it odds of the first participants being selected 
            # should equally high compared to others.
            if ws.select() == participants[INDEX_PRIVILEGED_PARTICIPANT]["id"]:
                num_first_selected_second_time += 1

        # The first participant should be selected in most cases (above threshold) in the first round 
        selected_percentage = num_first_selected // WeightedSelectionTest.FACTOR
        self.assertGreater(selected_percentage, WeightedSelectionTest.THRESHOLD, WeightedSelectionTest.UNFAIR_ERROR_MESSAGE)

        # In the second round the weight of the first participant should be reset and all participants
        # should have somewhat equal opportunities.
        self.assertLess(
            num_first_selected_second_time // WeightedSelectionTest.FACTOR, 
            (100 // WeightedSelectionTest.NUM_PARTICIPANTS) + WeightedSelectionTest.ERROR_MARGIN,
            WeightedSelectionTest.STILL_UNFAIR_ERROR_MESSAGE
        )


if __name__ == '__main__':
    unittest.main()
