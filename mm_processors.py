
import datetime
import discord
import re
import requests

from abc import ABC, abstractmethod, abstractproperty
from data import db
from random import choice
from types import NoneType
from youtubesearchpython import Video, VideosSearch


MAX_MINUTES = 5
PROFILE_PICTURE = 'profile_picture'


class ProcessResult(ABC):

    @abstractmethod
    def send(self, channel):
        pass


class ProcessResultEmbed(ProcessResult):

    def __init__(
            self, 
            title: str, 
            footer: str, 
            fields: dict = {}, 
            description: str = None, 
            url: str = None
        ):
        self.__embed = discord.Embed(
            title = title,
            description = description,
            color = 0x579B82,
            url = url
        )
        for key, value in fields.items():
            self.__embed.add_field(name=key, value=value)

        self.__embed.set_footer(text=footer)

    def send(self, channel):
        return channel.send(embed=self.__embed)


class ProcessResultText(ProcessResult):

    def __init__(self, text):
        self.text = text
    
        
    def send(self, channel):
        return channel.send(self.text)


class MusicRequestProcessor(ABC):
    

    def __init__(self, client):
        self.client = client

    @abstractproperty
    def command(self):
        pass

    @abstractmethod
    async def process(self, message):
        pass


class MusicRequestListProcessor(MusicRequestProcessor):

    last_titles = None

    @property
    def command(self):
        return '!mm'


    async def process(self, message):
        """
        This function is used to process the requests from the client. It will return the titles of the requests.
        @param self - the object itself
        @param _ - the parameter that is not used
        @returns the titles of the requests
        """
        if self.client.config.ultra_stealth:
            return ProcessResultText("Nothing to see here. Move along...")

        if len(self.client.requests) == 0:
            return ProcessResultText("No requests have been made...")
        else:
            requests = self.client.requests.values()
            total_weight = sum([r['weight'] for r in requests])
            if self.client.config.stealth_request:
                titles = [f"{index+1}. {r['name']} heeft een plaatje aangevraagd!" for index, r in enumerate(requests)]
            else:
                titles = [f"{index+1}. {r['name']} ({round((r['weight'] * 100)/ total_weight)}%): ***{r['title']}***" for index, r in enumerate(requests)]

            if titles == self.last_titles:
                await message.delete()
                return

            self.last_titles = titles

            return ProcessResultEmbed(
                title=f"Running in ***{self.client.config.mode}*** mode",
                fields={
                    "Requests": "\n".join(titles)
                },
                footer="Add music with !mm_add <title>"
            )


class MusicRequestAddProcessor(MusicRequestProcessor):

    @property
    def command(self):
        return '!mm_add'


    def __validate(self, video, message):
        duration = video.get('duration', MAX_MINUTES * 60 + 1)
        if isinstance(duration, NoneType):
            return False 
        if duration > MAX_MINUTES * 60:
            return False
        if db.get_user_request(self.client.connection, message.author.id):
            return False
        if db.in_blacklist(self.client.connection, video.get('link')):
            return False
        return True


    def __get_duration_in_seconds(self, duration):
        """
        Given a duration, return the duration in seconds.
        @param duration - the duration in seconds, or a dictionary containing the duration in text.
        @return the duration in seconds.
        """
        if type(duration) is dict:
            return int(duration.get('secondsText'))
        elif type(duration) is str:
            list = duration.split(":")
            if len(list) == 2:
                return int(list[0]) * 60 + int(list[1])
            elif len(list) == 3:
                return int(list[0]) * 3600 + int(list[1]) * 60 + int(list[2])


    async def process(self, message):
        """
        Validates the (video) request before adding it to the queue.
        @param message - the (video) request to add to the queue.
        @returns a message indicating the result of the validation process.
        """

        # TODO: Check if a selection has already been made.

        if message.channel.name != self.client.current_thread:
            return ProcessResultText(f'Please submit your requests in thread channel ({self.client.current_thread})')

        # The length of the command plus the space.
        req = message.content[len(self.command) + 1:].lstrip()
    
        # A YouTube URL has a video id of 11 letter/number/underscore/dash
        yt_url_regex = re.compile(r"(https://www.youtube.com/watch\?v=|https://youtu.be/)(?P<video_id>[\w-]{11})")
        yt_url = yt_url_regex.search(req)

        if yt_url: 
            try:
                req_result = Video.getInfo(yt_url.group('video_id'))
            except TypeError:
                return ProcessResultText('No video found!')
        else:
            videosSearch = VideosSearch(req, limit = 1)
            if videosSearch.result().get('result'):
                req_result = videosSearch.result()['result'][0]
            else:
                return ProcessResultText('No video found!')

        video = {
            "keyword": req,
            "title": req_result.get('title'),
            "user": message.author.name if message.author.nick == None else message.author.nick,
            "link": req_result.get('link'),
            "duration": self.__get_duration_in_seconds(req_result.get('duration')),
            "thumbnail": req_result.get('thumbnails')[0]
        }

        if self.__validate(video, message):
            if not db.user_exists(self.client.connection, message.author.id):
                db.insert_user(self.client.connection, message.author.id, video.get('user'), 10)
            db.insert_request(self.client.connection, message.author.id, video.get('title'), video.get('link'))

            if self.client.config.stealth_request:
                await message.delete()
                if not self.client.config.ultra_stealth:
                    return ProcessResultText(f'To {video.get("user")}: Great success!')
            else:
                return ProcessResultText(f'Great success! - {video.get("title")}: {video.get("link")}')
        else:
            if not self.client.config.ultra_stealth:
                return ProcessResultText(f'Great success! - NOT')


class MusicRequestRepeatProcessor(MusicRequestProcessor):

    @property
    def command(self):
        return '!mm_repeat'


    async def process(self, message):
        """
        Add a previous request to the queue.
        @param message - the request message that was send (where the user is derived from)
        @returns a message indicating whether the previous video was added or not.
        """
        previous_request = db.get_previous_request(self.client.connection, message.author.id, 1)
        if len(previous_request) == 0:
            return ProcessResultText("Such empty... 🤷🏽‍♂️")
        
        previous = previous_request[0]
        # TODO: Check if the song has not previously been selected.
        db.insert_request(self.client.connection, message.author.id, previous['title'], previous['link'])
        if self.client.config.stealth_request and not self.client.config.ultra_stealth:
            user = message.author.name if message.author.nick == None else message.author.nick
            return ProcessResultText(f'To {user}: 🔁 Great success!')
        else: 
            return ProcessResultText(f"🔁 Great success! - {previous['title']}: {previous['link']}")


class MusicRequestSuggestProcessor(MusicRequestProcessor):

    @property
    def command(self):
        return '!mm_suggest'


    async def process(self, message):
        list = self.client.suggestion_extension.request_suggestion(self.client.connection, message.author.id)
        suggestions = [f"{index+1}. ***{suggestion}***" for index, suggestion in enumerate(list)]
        return ProcessResultEmbed(
                title=f"***Suggestions***",
                fields={
                    "Songs": "\n".join(suggestions)
                },
                footer="Request suggestions with !mm_suggest <number>"
            )


class MusicRequestEndorseProcessor(MusicRequestProcessor):

    @property
    def command(self):
        return '!mm_endorse'


    async def process(self, message):
        """
        Endorse a request that has been added to the queue.
        @param message - the request message that was send (where the user is derived from)
        @returns a message indicating whether the previous video was added or not.
        """

        requests = self.client.requests
        if len(requests) == 0:
            return ProcessResultText("There are no songs to endorse!")

        if db.get_user_request(self.client.connection, message.author.id):
            return ProcessResultText("⛔️ You have already requested a song (so you cannot endorse another)")

        req = message.content[len(self.command) + 1:].lstrip()
        if not req.isdigit() or int(req) > len(requests):
            return ProcessResultText("⛔️ This is not a valid index")
        
        # HACK: Dictionary is orderless but we have an index so we are assuming order.
        request_list = [v for v in requests.values()]
        request = request_list[int(req)-1]
        db.insert_request(self.client.connection, message.author.id, request['title'], request['link'])
        if self.client.config.stealth_request and not self.client.config.ultra_stealth:
            user = message.author.name if message.author.nick == None else message.author.nick
            return ProcessResultText(f'To {user}: 🔗 Great success!')
        else: 
            return ProcessResultText(f"🔗 Great success! - You have endorsed {request['title']}: {request['link']}")

# 
# Protected processors
# 

class MusicRequestProtectedProcessor(MusicRequestProcessor):

    @abstractmethod
    async def protected_process(self, message):
        pass


    async def process(self, message):
        """
        This function is used to process the message. It checks to see if the message is from an admin.
        @param message - the message to be processed
        @returns the result of the process
        """
        for role in message.author.roles:
            if role.name == self.client.config.admin_role:
                result = await self.protected_process(message)
                return result
        else:
            return ProcessResultText("👮🏻‍♂️: Computer says no!")


class MusicRequestInitProcessor(MusicRequestProtectedProcessor):

    @property
    def command(self):
        return '!mm_init'


    def __get_current_thread(self, channel):
        """
        Get the current thread for the current day. If there is no thread return None.
        @param self - the client itself
        @param channel - the channel to post to
        @return the current thread or none
        """
        threads = self.client.discord_extension.get_threads(channel)
        name = datetime.datetime.now().strftime('%Y%m%d')
        for t in threads:
            if t['name'] == name:
                return t
        return None


    def __create_new_thread(self, channel):
        """
        Create a new thread in the discord channel.
        @param self - the client itself.
        @param channel - the channel to create the thread in.
        """
        name = datetime.datetime.now().strftime('%Y%m%d')
        return self.client.discord_extension.create_thread(name, channel)
    

    async def protected_process(self, message):
        thread = self.__get_current_thread(message.channel)
        if thread is None:
            thread = self.__create_new_thread(message.channel)
        self.client.current_thread = thread['name']
        return ProcessResultEmbed(
            title=f"Listening on thread {thread['name']}",
            description=f"Please submit your requests in this thread",
            url=f"https://discord.com/channels/{thread['guild_id']}/{thread['id']}",
            footer="Add music with !mm_add <title>"
        )


class MusicRequestRejectProcessor(MusicRequestProtectedProcessor):

    @property
    def command(self):
        return '!mm_reject'


    async def protected_process(self, message):
        """
        This function is used to reject a request.
        @returns the response to the message
        """
        requests = self.client.requests
        if len(requests) == 0:
            return ProcessResultText("There are no songs to reject!")

        req = message.content[len(self.command) + 1:].lstrip()
        if not req.isdigit() or int(req) > len(requests):
            return ProcessResultText("⛔️ This is not a valid index")

        # HACK: Dictionary is orderless but we have an index so we are assuming order.
        request_list = [v for v in requests.values()]
        request = request_list[int(req)-1]
        db.delete_request(self.client.connection, request["id"])
        # TODO: Parameterize whether the song should also be blacklisted
        db.insert_blacklist(self.client.connection, request["title"], request["link"])
        return ProcessResultText("And its gone...")


class MusicRequestWeightsProcessor(MusicRequestProtectedProcessor):

    @property
    def command(self):
        return '!mm_weights'


    async def protected_process(self, message):
        users = db.get_users_by_weights(self.client.connection)

        if self.client.config.ultra_stealth:
            return ProcessResultText("Nothing to see here. Move along...")

        if len(users) == 0:
            return ProcessResultText("No active users...")
        else:
            users = [f"{index+1}. {u['nick_name']}: ***{u['weight']}***" for index, u in enumerate(users)]
            return ProcessResultEmbed(
                title=f"Currently there are {len(users)} active users",
                fields={
                    "Users": "\n".join(users)
                },
                footer="Get your weight up, by requesting songs!"
            )



class SongTitleSimilarity:
    RECURSION_DEPTH = 3

    def __init__(self, titles) -> None:
        self.__titles = titles


    def find_closest(self, title):
        print(title)
        d = {}
        s1 = self.__sanitize(title.casefold())
        print(s1)
        for title in self.__titles:
            s2 = self.__sanitize(title.casefold())
            d[title] = self.__ratcliff_obershelp(s1, s2)
        sortdict = sorted(d.items(), key=lambda x:x[1], reverse=True)
        return sortdict[0]


    def __sanitize(self, string):
        """
        This function tries to remove some "unused" character to get a 
        better metric.
        """ 
        return string \
            .replace(" - ", " ").replace("- ", " ").replace(" -", " ") \
            .replace(" | ", " ").replace("| ", " ").replace(" |", " ")


    def __ratcliff_obershelp(self, s1, s2):
        length = self.__longest_common_substring_recursive(s1, s2, SongTitleSimilarity.RECURSION_DEPTH)
        return 2 * length / (len(s1) + len(s2))
        

    def __longest_common_substring_recursive(self, s1, s2, count):
        length = 0 
        if count == 0 or len(s1) == 0 or len(s2) == 0:
            return length

        (i,j), l = self.__longest_common_substring(s1,s2)
        length = l
        if l > 0:
            length += self.__longest_common_substring_recursive(
                s1.replace(s1[i - l + 1: i + 1], ""), 
                s2.replace(s2[j - l + 1: j + 1], ""),
                count - 1
            )

        return length


    def __longest_common_substring(self, s1, s2):
        dict =  {}
        # For every character in the first string compare it to 
        # all characters in the second string.
        for i in range(len(s1)):
            for j in range(len(s2)):
                # For every match we store the index for the position in 
                # the first and previous string. To check a sequence of 
                # matching characters we simply increment the previous
                # matching count (i-1 and j-1) - if available. 
                if s1[i] == s2[j]:
                    current_count = 0
                    if (i-1, j-1) in dict:
                        current_count = dict[(i-1, j-1)] 
                    dict[(i,j)] = current_count + 1
        # The dictionary contains the characters and the count of matching 
        # sequence of characters. We sort it by the count (desc) and get
        # the first with the length of the sequence and location of the 
        # last character in the two strings (position i in the first string
        # and position j in seconds sting).
        if dict == {}:
            return (0,0), 0
        sortdict = sorted(dict.items(), key=lambda x:x[1], reverse=True)
        return  sortdict[0] # (i, j), length


class MusicRequestSimilarityProcessor(MusicRequestProtectedProcessor):

    @property
    def command(self):
        return '!mm_sims'


    async def protected_process(self, message):
        if len(self.client.requests) == 0:
            return ProcessResultText("No requests have been made...")
        
        titles = []
        history = db.get_history(self.client.connection)
        sims = SongTitleSimilarity([h[1] for h in history])
        for index, (_, request) in enumerate(self.client.requests.items()):
            closest_match = sims.find_closest(request.get("title"))
            titles.append(f"{index+1}. ***{request.get('title')}*** : ({round(closest_match[1], 3)}) - {closest_match[0]}")
        return ProcessResultEmbed(
            title=f"Running in ***{self.client.config.mode}*** mode",
            fields={
                "Request similarities": "\n".join(titles)
            },
            footer="Highly experimental!"
        )
    

class WeightedSelection:

    def __init__(self):
        self._participants = []


    @property
    def participants(self):
        return self._participants


    def add_participant(self, participant_id, weight=1):
        """
        This function adds a participant to the list of participants. If present the participants previous weight is included.
        @param self - the object itself
        @param participant_id - the participant id 
        """
        self._participants.append({ "id": participant_id, "weight": weight })


    def select(self):
        """
        This function is used to make a weighted selection for a winning participant. 
        It will return the discord id of the participant for which the request is granted.
        @param self - the object itself
        @returns the id of the winning user
        """
        expanded_list = []
        # Create an expanded list (from which to select a participant) and increment all weights
        for participant in self._participants:
            for _ in range(participant["weight"]):
                expanded_list.append(participant)

        # Select from the expanded ticket list
        selected = choice(expanded_list)

        # Update weight of the selected participant
        for participant in self._participants:
            if participant["id"] == selected["id"]:
                participant["weight"] = 0
                break

        return selected["id"]


class MusicRequestSelectProcessor(MusicRequestProtectedProcessor):

    @property
    def command(self):
        return 'When the crowd says bot!'


    def __download_image(self, url):
        r = requests.get(url)
        with open(PROFILE_PICTURE, 'wb') as f:
            f.write(r.content) 


    async def protected_process(self, message):
        """
        This function is used to process the message. It is called by the on_message function.
        @returns the selected song.
        """
        
        # TODO: check if there's already a request in history for today?

        if not self.client.config.birthday:
            if len(self.client.requests) == 0:
                return ProcessResultText("No requests have been made...")
            else:
                # Make a weighted selection.
                weighted = WeightedSelection()
                for request in self.client.requests.values():
                    weighted.add_participant(request['discord_id'], request['weight'])
                selected_discord_id = weighted.select()
                
                # Persist the new weights and selection to the database.
                selected = db.get_user_request(self.client.connection, selected_discord_id)
                db.update_user_weight(self.client.connection, weighted.participants)
                db.reset_user_weight(self.client.connection, selected_discord_id)                
                db.insert_history(self.client.connection, selected['id'])

                # Update the bot's profile picture.
                # self.__download_image(selected.get("thumbnail").get("url"))
                # fp = open(PROFILE_PICTURE, 'rb')
                # await client.user.edit(avatar=fp.read())
                return ProcessResultText(f"SELECTA: {selected['yt_title']} - {selected['yt_link']}")
        else:
            return ProcessResultText(f"SELECTA: Happy Birthday to {self.client.config.party_animal}!! - {db.get_birthday_song(self.client.connection)['yt_link']}")

